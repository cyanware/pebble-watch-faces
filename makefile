PROJECT := cyborg
CREATE  := ~/pebble-dev/pebble-sdk-release-001/tools/create_pebble_project.py
SDK     := ~/pebble-dev/pebble-sdk-release-001/sdk
INSTALL := ~/pebble-dev/watches

all:
	cd .. && ./waf build

cyborg:
	${CREATE} ${SDK} ${PROJECT}
	cp makefile ${PROJECT}

lunch-hour:
	${CREATE} ${SDK} $@
	cp makefile $@/src/.

configure:
	cd .. && ./waf configure

clean:
	cd .. && ./waf clean

install:
	cd ../build && python -m SimpleHTTPServer 8000
