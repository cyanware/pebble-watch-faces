//
// Copyright 2013 CYANware Software Solutions
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "ProgressBar.h"

static Layer bar;
static int progressStartTime;
static float nextScheduledPulse;

static int timestamp()
{
    // Calculate the number of seconds elapsed since the beginning of the year
    PblTm t;
    get_time(&t);

    return t.tm_yday * 24 * 60 * 60 + t.tm_hour * 60 * 60 + t.tm_min * 60 + t.tm_sec;
}

static float getProgressPercent(int seconds)
{
    return 1.0 * (timestamp() - progressStartTime) / seconds;
}

static void progressBarUpdate(Layer *layer, GContext *ctx)
{
    if (layer_get_hidden(&bar)) {
        return;
    }

    // What percentage of an hour has elapsed since we started counting
    float progress = getProgressPercent(60 * 60);
    if (progress >= 1.0) {
        static const uint32_t const segments[] = { 500, 100, 500, 100, 500 };
        VibePattern pattern = {
            .durations = segments,
            .num_segments = ARRAY_LENGTH(segments)
        };
        vibes_enqueue_custom_pattern(pattern);
        layer_set_hidden(&bar, true);
        layer_mark_dirty(&bar);
    }
    else if (progress >= nextScheduledPulse) {
        vibes_long_pulse();
        nextScheduledPulse += 0.25;
    }

    // Draw the initial progress bar outline and mark each 25% segment
    graphics_context_set_stroke_color(ctx, GColorWhite);
    GPoint points[] = { GPoint(0, 0),
                        GPoint(layer->frame.size.w / 4 - 1, 0),
                        GPoint(layer->frame.size.w / 4 - 1, layer->frame.size.h - 1),
                        GPoint(0, layer->frame.size.h - 1) };
    GPathInfo info = {
        .points = points,
        .num_points = ARRAY_LENGTH(points)
    };

    GPath path;
    gpath_init(&path, &info);
    for (int i = 0; i < 4; ++i) {
        gpath_move_to(&path, GPoint(layer->frame.size.w * i / 4, 0));
        gpath_draw_outline(ctx, &path);
    }

    // Fill up the progress bar
    graphics_context_set_fill_color(ctx, GColorWhite);
    graphics_fill_rect(ctx, GRect(0, 0, progress * layer->frame.size.w + 0.5, layer->frame.size.h), 0, GCornerNone);
}

//------------------------------------------------------------------------

Layer *progressBarInit(GRect frame)
{
    layer_init(&bar, frame);
    bar.update_proc = progressBarUpdate;
    layer_set_hidden(&bar, true);

    return &bar;
}

void setProgressBarEnabled(bool enabled)
{
    layer_set_hidden(&bar, !enabled);
    layer_mark_dirty(&bar);
    if (enabled) {
        progressStartTime = timestamp();
        nextScheduledPulse = 0.25;
    }
}

bool getProgressBarEnabled()
{
    return !layer_get_hidden(&bar);
}
