//
// Copyright 2013 CYANware Software Solutions
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "debug.h"

static TextLayer debugTextLayer;
static char debugText[1024];

//------------------------------------------------------------------------

static void onSingleClick(ClickRecognizerRef recognizer, Window *window)
{
    ButtonId sender = click_recognizer_get_button_id(recognizer);

    switch (sender) {
        case BUTTON_ID_BACK:
            // Reserved for popping the top window
            vibes_double_pulse();
            break;
        case BUTTON_ID_UP:
            vibes_short_pulse();
            break;
        case BUTTON_ID_SELECT:
            layer_set_hidden(&debugTextLayer.layer, !debugTextLayer.layer.hidden);
            break;
        case BUTTON_ID_DOWN:
            vibes_double_pulse();
            break;
        default:
            break;
    }
}

static void clickConfiguration(ClickConfig **config, Window *window)
{
    (void)window;

    config[BUTTON_ID_UP]->click.handler = (ClickHandler)onSingleClick;
    config[BUTTON_ID_SELECT]->click.handler = (ClickHandler)onSingleClick;
    config[BUTTON_ID_DOWN]->click.handler = (ClickHandler)onSingleClick;
}

//------------------------------------------------------------------------

void dinit(Window *window)
{
    // Hide the status bar to simulate a fullscreen watchface
    window_set_fullscreen(window, true);

    text_layer_init(&debugTextLayer, window->layer.frame);
    text_layer_set_text_color(&debugTextLayer, GColorBlack);
    text_layer_set_background_color(&debugTextLayer, GColorWhite);
    text_layer_set_font(&debugTextLayer, fonts_get_system_font(FONT_KEY_GOTHIC_14));
    text_layer_set_text_alignment(&debugTextLayer, GTextAlignmentLeft);

    // Hide the debug layer initially
    layer_set_hidden(&debugTextLayer.layer, true);
    layer_add_child(&window->layer, &debugTextLayer.layer);

    // Configure button click handlers
    window_set_click_config_provider(window, (ClickConfigProvider)clickConfiguration);
}

void dlog(const char *msg)
{
    // Check for End-of-Text string and buffer overflow
    if (strcmp(msg, "EOT") == 0 || strlen(debugText) + strlen(msg) + 1 >= 1024) {
        strcpy(debugText, "");
    }
    else {
        // Concatenate the message string to the debug text buffer
        strcat(debugText, "\n");
        strcat(debugText, msg);
    }
    text_layer_set_text(&debugTextLayer, debugText);
}
