//
// Copyright 2013 CYANware Software Solutions
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#pragma once

#include "pebble_os.h"
#include "pebble_app.h"
#include "pebble_fonts.h"

//#define DEBUG // uncomment to access debugging mode

#ifdef DEBUG
#define DINIT(window) dinit((window))
#define DLOG(msg) dlog((msg))
#else
#define DINIT(window)
#define DLOG(msg)
#endif

extern void dinit(Window *window);
extern void dlog(const char *msg);
