//
// Copyright 2013 CYANware Software Solutions
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "pebble_os.h"
#include "pebble_app.h"
#include "pebble_fonts.h"

#include "debug.h"
#include "ProgressBar.h"


//#define WATCH_APP  // Uncomment to compile a watch app instead of a watch face


#define MY_UUID { 0x4E, 0x14, 0xDD, 0x21, 0xF6, 0xF3, 0x43, 0x45, 0x8E, 0x8A, 0x6B, 0x20, 0x68, 0x0A, 0xE5, 0xB0 }
PBL_APP_INFO(MY_UUID,
             "Lunch Hour", "CYANware Software Solutions",
             1, 0, /* App version */
             RESOURCE_ID_IMAGE_MENU_ICON,
#if defined(DEBUG) || defined(WATCH_APP)
             APP_INFO_STANDARD_APP);
#else
             APP_INFO_WATCH_FACE);
#endif

static Window window;
static GFont font;
static TextLayer label;
static bool firstTick = true;
static char timeText[] = "00:00:00a";           // 12:34:00a
static char weekdayText[] = "Wednesday";        // Longest weekday string
static char dateText[] = "September 30";        // Longest date string
static char weekdayDateText[] = "Wed, Sep 30";  // Abbreviated weekday and date

const GRect screen = {
    .origin = { .x = 0, .y = 0 },
    .size = { .w = 144, .h = 168 }
};

//------------------------------------------------------------------------

void backgroundUpdate(Layer *layer, GContext *ctx)
{
    // Draw a white-filled circle that's the full-width of the screen, 
    // flush with the bottom edge of the screen
    graphics_context_set_fill_color(ctx, GColorWhite);
    GPoint center = grect_center_point(&(layer->frame));
    int16_t offsetY = (screen.size.h - screen.size.w) / 2;
    center.y += offsetY;
    graphics_fill_circle(ctx, center, center.x);

    // Draw tick marks for every minute
    graphics_context_set_stroke_color(ctx, GColorBlack);
    GPoint minutePoints[] = { GPoint(0, 0),
                              GPoint(screen.size.w, 0),
                              GPoint(screen.size.w, 0),
                              GPoint(0, 0)
    };
    GPathInfo stroke = {
        .num_points = 4
    };
    stroke.points = minutePoints;

    GPath path;
    gpath_init(&path, &stroke);
    gpath_move_to(&path, center);
    for (int i = 0; i < 60; ++i) {
        gpath_rotate_to(&path, i * 0x10000 / 60);
        gpath_draw_outline(ctx, &path);
    }
    graphics_fill_circle(ctx, center, center.x - 5);

    // Draw tick marks for every hour
    graphics_context_set_fill_color(ctx, GColorBlack);
    GPoint hourPoints[] = { GPoint(0, -3),
                            GPoint(screen.size.w, -3),
                            GPoint(screen.size.w, 3),
                            GPoint(0, 3)
    };
    stroke.points = hourPoints;
    gpath_init(&path, &stroke);
    gpath_move_to(&path, center);
    for (int i = 0; i < 12; ++i) {
        gpath_rotate_to(&path, i * 0x10000 / 12);
        gpath_draw_outline(ctx, &path);
        gpath_draw_filled(ctx, &path);
    }
    graphics_context_set_fill_color(ctx, GColorWhite);
    graphics_fill_circle(ctx, center, center.x - 10);
}

//------------------------------------------------------------------------

void hourHandUpdate(Layer *layer, GContext *ctx)
{
    graphics_context_set_fill_color(ctx, GColorBlack);

    GPoint center = grect_center_point(&(layer->frame));
    int16_t offsetY = (screen.size.h - screen.size.w) / 2;
    center.y += offsetY;

    graphics_context_set_stroke_color(ctx, GColorBlack);
    GPoint points[] = { GPoint(-10, 0),
                        GPoint(0, -45),
                        GPoint(10, 0),
                        GPoint(0, 20) };
    GPathInfo info = {
        .points = points,
        .num_points = ARRAY_LENGTH(points)
    };

    PblTm time;
    get_time(&time);

    GPath path;
    gpath_init(&path, &info);
    gpath_move_to(&path, center);
    // Calculate the angle where positive values are clockwise
    int angle = (time.tm_hour % 12 + time.tm_min / 60.0) / 12.0 * TRIG_MAX_ANGLE;
    gpath_rotate_to(&path, angle);
    gpath_draw_filled(ctx, &path);
}

void minuteHandUpdate(Layer *layer, GContext *ctx)
{
    graphics_context_set_fill_color(ctx, GColorBlack);

    GPoint center = grect_center_point(&(layer->frame));
    int16_t offsetY = (screen.size.h - screen.size.w) / 2;
    center.y += offsetY;

    graphics_context_set_stroke_color(ctx, GColorBlack);
    GPoint points[] = { GPoint(-10, 0),
                        GPoint(0, -65),
                        GPoint(10, 0),
                        GPoint(0, 20) };
    GPathInfo info = {
        .points = points,
        .num_points = ARRAY_LENGTH(points)
    };

    PblTm time;
    get_time(&time);

    GPath path;
    gpath_init(&path, &info);
    gpath_move_to(&path, center);
    // Calculate the angle where positive values are clockwise
    int angle = (time.tm_min + time.tm_sec / 60.0) / 60.0 * TRIG_MAX_ANGLE;
    gpath_rotate_to(&path, angle);
    gpath_draw_filled(ctx, &path);

    graphics_context_set_fill_color(ctx, GColorWhite);
    graphics_fill_circle(ctx, center, 3);
}

//------------------------------------------------------------------------

static void onSingleClick(ClickRecognizerRef recognizer, Window *window)
{
    ButtonId sender = click_recognizer_get_button_id(recognizer);

    switch (sender) {
        case BUTTON_ID_BACK:
            // Reserved for popping the top window
            break;
        case BUTTON_ID_UP:
            break;
        case BUTTON_ID_SELECT: {
            // Toggle the progress bar
            bool enabled = !getProgressBarEnabled();
            setProgressBarEnabled(enabled);
            if (enabled) {
                text_layer_set_text(&label, "W I L L   R E T U R N");
            }
            else {
                text_layer_set_text(&label, weekdayDateText);
            }
            // Force text label to update with the new text value
            firstTick = true;
            break;
        }
        case BUTTON_ID_DOWN:
            break;
        default:
            break;
    }
}

static void clickConfiguration(ClickConfig **config, Window *window)
{
    (void)window;

    config[BUTTON_ID_UP]->click.handler = (ClickHandler)onSingleClick;
    config[BUTTON_ID_SELECT]->click.handler = (ClickHandler)onSingleClick;
    config[BUTTON_ID_DOWN]->click.handler = (ClickHandler)onSingleClick;
}

//------------------------------------------------------------------------

void handleInit(AppContextRef ctx)
{
    (void)ctx;

    window_init(&window, "Lunch Hour");
    window_stack_push(&window, true /* Animated */);

    window_set_background_color(&window, GColorBlack);

    // Draw the watch face background
    static Layer background;
    layer_init(&background, window.layer.frame);
    background.update_proc = backgroundUpdate;
    layer_add_child(&window.layer, &background);

    // Add a static text label
    GRect frame = window.layer.frame;
    frame.origin.y += 2;
    frame.size.h = screen.size.h - screen.size.w;
    text_layer_init(&label, frame);
    font = fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD);
    text_layer_set_font(&label, font);
    text_layer_set_background_color(&label, GColorClear);
    text_layer_set_text_color(&label, GColorWhite);
    text_layer_set_text_alignment(&label, GTextAlignmentCenter);
    layer_add_child(&window.layer, &label.layer);

    // Initialize a progress bar
    frame = window.layer.frame;
    frame.size.h = 4;
    Layer *progressBar = progressBarInit(frame);
    layer_add_child(&window.layer, progressBar);

    // Add a hour hand
    static Layer hourHand;
    layer_init(&hourHand, window.layer.frame);
    hourHand.update_proc = hourHandUpdate;
    layer_add_child(&window.layer, &hourHand);

    // Add a minute hand
    static Layer minuteHand;
    layer_init(&minuteHand, window.layer.frame);
    minuteHand.update_proc = minuteHandUpdate;
    layer_add_child(&window.layer, &minuteHand);
    
    DINIT(&window);
    DLOG("app did finish launching");

#ifdef WATCH_APP
    // Configure button click handlers for watch app only
    window_set_click_config_provider(&window, (ClickConfigProvider)clickConfiguration);
    setProgressBarEnabled(false);
#else
    setProgressBarEnabled(true);
#endif
}

void handleDeinit(AppContextRef ctx)
{
    (void)ctx;

    DLOG("app will terminate");
}

void handleTickEvent(AppContextRef ctx, PebbleTickEvent *t)
{
    (void)ctx;

    if (firstTick || t->units_changed & DAY_UNIT) {
        // Update the weekday and date layers
        string_format_time(weekdayText, sizeof(weekdayText), "%A", t->tick_time);
        DLOG(weekdayText);
        string_format_time(dateText, sizeof(dateText), "%B %e", t->tick_time);
        DLOG(dateText);
        string_format_time(weekdayDateText, sizeof(weekdayDateText), "%a, %h %e", t->tick_time);
        DLOG(weekdayDateText);
        if (getProgressBarEnabled()) {
            text_layer_set_text(&label, "W I L L   R E T U R N");
        }
        else {
            text_layer_set_text(&label, weekdayDateText);
        }
    }
    if (firstTick || t->units_changed & HOUR_UNIT) {
        // Update the hour layer
        string_format_time(timeText, sizeof(timeText), "%I:00:00", t->tick_time);
        DLOG(timeText);
    }
    if (firstTick || t->units_changed & MINUTE_UNIT) {
        // Update the minute layer
        layer_mark_dirty(&window.layer);
        string_format_time(timeText, sizeof(timeText), "00:%M:00", t->tick_time);
        DLOG(timeText);
    }
    if (firstTick || t->units_changed & SECOND_UNIT) {
        // Update the second layer
        string_format_time(timeText, sizeof(timeText), "00:00:%S", t->tick_time);
        DLOG(timeText);
        // If the progress has reached 100%, then revert back to the date text
        if (!getProgressBarEnabled() && strcmp(text_layer_get_text(&label), "W I L L   R E T U R N") == 0) {
            text_layer_set_text(&label, weekdayDateText);
        }
    }

    firstTick = false;
}

//------------------------------------------------------------------------

void pbl_main(void *params)
{
    PebbleAppHandlers handlers = {
        .init_handler = &handleInit,
        .deinit_handler = &handleDeinit,
        .tick_info = {
            .tick_handler = &handleTickEvent,
#ifdef DEBUG
            .tick_units = SECOND_UNIT
#else
            .tick_units = MINUTE_UNIT
#endif
        }
    };
    app_event_loop(params, &handlers);
}
