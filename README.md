# Welcome

This is my quick-and-dirty repo for watch faces developed using the Pebble Proof-of-Concept SDK.

To access the source code:

```
$ git clone https://bitbucket.org/cyanware/pebble-watch-faces.git
```

All source code is licensed under the Apache License 2.0.

If you're only interested in the .pbw files, go to the Downloads section, or even better, visit the website
[http://mypebblefaces.com](http://mypebblefaces.com) where you will find many more watch faces from other developers.

