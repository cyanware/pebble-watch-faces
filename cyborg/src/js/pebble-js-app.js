// Initialization event

Pebble.addEventListener("ready",
                        function(e) {
                            console.log("Connected: " + e.ready);
                            console.log(e.type);
                        });

// Pebble app configuration window events

Pebble.addEventListener("showConfiguration",
                        function(e) {
                            //var url = "http://10.0.1.12/~cyborg/configuration.html?";
                            var url = "http://cyanware-pebble-apps.parseapp.com/cyborg/configuration.html?";
                            var item = window.localStorage.getItem("options");
                            if (item !== undefined && item !== null) {
                                var options = JSON.parse(item);
                                console.log("Saved configuration: " + JSON.stringify(options));
                                Pebble.openURL(url +
                                               "screen-saver=" + options["screen-saver"] +
                                               "&checkbox-uptime=" + options["checkbox-uptime"] +
                                               "&checkbox-battery-percentage=" + options["checkbox-battery-percentage"] +
                                               "&checkbox-battery-icon=" + options["checkbox-battery-icon"]
                                    );
                            }
                            else {
                                Pebble.openURL(url);
                            }
                        });

Pebble.addEventListener("webviewclosed",
                        function(e) {
                            if (e.response.length > 0) {
                                var options = JSON.parse(decodeURIComponent(e.response));
                                console.log("Configuration window returned: " + JSON.stringify(options));
                                window.localStorage.setItem("options", JSON.stringify(options));
                                Pebble.sendAppMessage(options,
                                                      function(e){
                                                          console.log("Successfully sent configuration to Pebble");
                                                      },
                                                      function(e){
                                                          console.log("Failed to send configuration to Pebble.");
                                                      }
                                    );
                            }
                        });
