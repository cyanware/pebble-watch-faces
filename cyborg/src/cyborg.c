//
// Copyright 2013 CYANware Software Solutions
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include <ctype.h>
#include <pebble.h>

// Uncomment for debug mode
//#define DEBUG 1

#ifdef DEBUG
#define DLOG(fmt, ...) app_log(APP_LOG_LEVEL_DEBUG, __FILE__, __LINE__, fmt, __VA_ARGS__)
#else
#define DLOG(fmt, ...)
#endif

Window *window;
TextLayer *statusLabel = NULL;
TextLayer *batteryLabel = NULL;
Layer *batteryIcon = NULL;
TextLayer *weekdayLabel = NULL;
TextLayer *dateLabel = NULL;
TextLayer *timeLabel = NULL;
bool useScreenSaver = true;
bool showUptime = true;
bool showBatteryPercent = true;
bool showBatteryIcon = true;

const GRect screenFrame = {
    .origin = { .x = 0, .y = 0 },
    .size = { .w = 144, .h = 168 }
};

const GRect statusBarFrame = {
    .origin = { .x = 4, .y = 0 },
    .size = { .w = 144 - 8, .h = 16 }
};

enum {
    PERSIST_TIMESTAMP
};

//------------------------------------------------------------------------
/*
void onSingleClick(ClickRecognizerRef recognizer, Window *window)
{
    ButtonId sender = click_recognizer_get_button_id(recognizer);

    switch (sender) {
        case BUTTON_ID_BACK:
            // Reserved for popping the top window
            vibes_double_pulse();
            break;
        case BUTTON_ID_UP:
            vibes_short_pulse();
            break;
        case BUTTON_ID_SELECT:
            vibes_long_pulse();
            break;
        case BUTTON_ID_DOWN:
            vibes_double_pulse();
            break;
        default:
            break;
    }
}

void clickConfiguration(ClickConfig **config, Window *window)
{
    (void)window;

    config[BUTTON_ID_UP]->click.handler = (ClickHandler)onSingleClick;
    config[BUTTON_ID_SELECT]->click.handler = (ClickHandler)onSingleClick;
    config[BUTTON_ID_DOWN]->click.handler = (ClickHandler)onSingleClick;
}
*/
void presentWindow(Window *window)
{
    window_stack_push(window, true);
    //vibes_long_pulse();
}

Window *dismissWindow()
{
    //vibes_long_pulse();
    return window_stack_pop(true);
}

//------------------------------------------------------------------------
//
// Screen Saver handling
//

Window *screenSaver = NULL;
Layer *chrome = NULL;
TextLayer *label = NULL;

void screenSaverUpdate(Layer *layer, GContext *ctx)
{
    GRect frame = layer_get_frame(layer);
    graphics_context_set_stroke_color(ctx, GColorBlack);

    // Draw concentric circles
    graphics_draw_circle(ctx, grect_center_point(&frame), 9);
    graphics_draw_circle(ctx, grect_center_point(&frame), 11);
    graphics_draw_circle(ctx, grect_center_point(&frame), 13);

    graphics_draw_circle(ctx, grect_center_point(&frame), 29);
    graphics_draw_circle(ctx, grect_center_point(&frame), 37);
    graphics_draw_circle(ctx, grect_center_point(&frame), 45);

    graphics_draw_circle(ctx, grect_center_point(&frame), 60);
    graphics_draw_circle(ctx, grect_center_point(&frame), 61);
    graphics_draw_circle(ctx, grect_center_point(&frame), 62);

    // Draw small cross hairs in each corner
    graphics_draw_circle(ctx, GPoint(15, 25), 10);
    graphics_draw_line(ctx, GPoint(0, 25), GPoint(30, 25));
    graphics_draw_line(ctx, GPoint(15, 10), GPoint(15, 40));

    graphics_draw_circle(ctx, GPoint(144 - 15, 25), 10);
    graphics_draw_line(ctx, GPoint(144 - 0, 25), GPoint(144 - 30, 25));
    graphics_draw_line(ctx, GPoint(144 - 15, 10), GPoint(144 - 15, 40));

    graphics_draw_circle(ctx, GPoint(15, 168 - 25), 10);
    graphics_draw_line(ctx, GPoint(0, 168 - 25), GPoint(30, 168 - 25));
    graphics_draw_line(ctx, GPoint(15, 168 - 10), GPoint(15, 168 - 40));

    graphics_draw_circle(ctx, GPoint(144 - 15, 168 - 25), 10);
    graphics_draw_line(ctx, GPoint(144 - 0, 168 - 25), GPoint(144 - 30, 168 - 25));
    graphics_draw_line(ctx, GPoint(144 - 15, 168 - 10), GPoint(144 - 15, 168 - 40));

    // Draw large cross hairs in the center
    graphics_draw_line(ctx, GPoint(4, 168 / 2 - 4), GPoint(144 - 4, 168 / 2 - 4));
    graphics_draw_line(ctx, GPoint(4, 168 / 2 - 2), GPoint(144 - 4, 168 / 2 - 2));
    graphics_draw_line(ctx, GPoint(4, 168 / 2), GPoint(144 - 4, 168 / 2));
    graphics_draw_line(ctx, GPoint(4, 168 / 2 + 2), GPoint(144 - 4, 168 / 2 + 2));
    graphics_draw_line(ctx, GPoint(4, 168 / 2 + 4), GPoint(144 - 4, 168 / 2 + 4));
    graphics_draw_line(ctx, GPoint(144 / 2 - 4, (168 - 144) / 2 + 4), GPoint(144 / 2 - 4, (168 - 144) / 2 + 144 - 4));
    graphics_draw_line(ctx, GPoint(144 / 2 - 2, (168 - 144) / 2 + 4), GPoint(144 / 2 - 2, (168 - 144) / 2 + 144 - 4));
    graphics_draw_line(ctx, GPoint(144 / 2, (168 - 144) / 2 + 4), GPoint(144 / 2, (168 - 144) / 2 + 144 - 4));
    graphics_draw_line(ctx, GPoint(144 / 2 + 2, (168 - 144) / 2 + 4), GPoint(144 / 2 + 2, (168 - 144) / 2 + 144 - 4));
    graphics_draw_line(ctx, GPoint(144 / 2 + 4, (168 - 144) / 2 + 4), GPoint(144 / 2 + 4, (168 - 144) / 2 + 144 - 4));

    // Hollow out the center
    graphics_context_set_fill_color(ctx, GColorWhite);
    graphics_fill_circle(ctx, grect_center_point(&frame), 8);
}

void screenSaverInit()
{
    screenSaver = window_create();
    window_set_fullscreen(screenSaver, true);

    Layer *rootLayer = window_get_root_layer(screenSaver);
    GRect rootFrame = layer_get_frame(rootLayer);

    window_set_background_color(screenSaver, GColorWhite);

    chrome = layer_create(rootFrame);
    layer_set_update_proc(chrome, screenSaverUpdate);
    layer_add_child(rootLayer, chrome);

    label = text_layer_create(rootFrame);
    text_layer_set_background_color(label, GColorClear);
    text_layer_set_text_alignment(label, GTextAlignmentCenter);
    text_layer_set_text(label, "Please Stand By");
    layer_add_child(rootLayer, text_layer_get_layer(label));
}

void screenSaverDeinit()
{
    window_destroy(screenSaver);
    layer_destroy(chrome);
    text_layer_destroy(label);
}

//------------------------------------------------------------------------
//
// Accelerometer tap handling
//

void handle_tick_event(struct tm *, TimeUnits);
void batteryStateHandler(BatteryChargeState);

void updateTime(void *data)
{
    time_t now = time(NULL);
    handle_tick_event(localtime(&now), SECOND_UNIT);
}

void presentWindowAfterDelay(void *data)
{
    presentWindow((Window *)data);
}

void accelTapHandler(AccelAxisType axis, int32_t direction)
{
    // If the Screen Saver mode is active, wake up and display the current time for 5 seconds
    if (window_stack_get_top_window() == screenSaver) {
        window_stack_pop(false);
        app_timer_register(1 * 1000, updateTime, NULL);
        app_timer_register(5 * 1000, presentWindowAfterDelay, screenSaver);
    }
    else {
        // Update the battery uptime status
        BatteryChargeState state = battery_state_service_peek();
        batteryStateHandler(state);
    }
}

//------------------------------------------------------------------------
//
// Battery state handling
//

time_t timestamp;
char uptime[] = "000d 00h 00m";
char format[] = "%%dd 00h 00m";
char level[] = "*100%";

void batteryStateHandler(BatteryChargeState state)
{
    DLOG("charge_percent = %d, is_charging = %d, is_plugged = %d", state.charge_percent, state.is_charging, state.is_plugged);
    int chargePercent = state.charge_percent;
    if (state.is_plugged) {
        if (state.is_charging) {
            snprintf(level, sizeof(level), "*%d%%", chargePercent);
        }
        else {
            snprintf(level, sizeof(level), "%d%%", chargePercent);
        }
        text_layer_set_text(batteryLabel, level);
        // Save the current time when we've reached a full charge
        if (chargePercent == 100) {
            time_t now = time(NULL);
            int32_t savedValue = persist_read_int(PERSIST_TIMESTAMP);
            // To minimize the number of times we write to flash memory, don't save the new value unless it's more than 1 hour difference
            if (now - savedValue > 60 * 60) {
                time(&timestamp);
                persist_write_int(PERSIST_TIMESTAMP, timestamp);
            }
        }
    }

    time_t elapsedTime = time(NULL) - timestamp;
    struct tm *t = localtime(&elapsedTime);
    strftime(format, sizeof(format), "%%dd %Hh %Mm", t);
    if (showUptime) {
        snprintf(uptime, sizeof(uptime), format, t->tm_yday);
    }
    else {
        strcpy(uptime, "");
    }
    text_layer_set_text(statusLabel, uptime);
    //text_layer_set_text(statusLabel, "1d 23h 45m");
    if (showBatteryPercent) {
        snprintf(level, sizeof(level), "%d%%", chargePercent);
    }
    else {
        strcpy(level, "");
    }
    text_layer_set_text(batteryLabel, level);
    layer_mark_dirty(batteryIcon);
}

void batteryIconUpdate(Layer *layer, GContext *ctx)
{
    GRect frame = layer_get_frame(layer);
    if (showBatteryIcon) {
        graphics_context_set_stroke_color(ctx, GColorWhite);
        graphics_context_set_fill_color(ctx, GColorWhite);
    }
    else {
        graphics_context_set_stroke_color(ctx, GColorBlack);
        graphics_context_set_fill_color(ctx, GColorBlack);
    }
    
    // Battery icon border
    GPathInfo border = {
        .num_points = 4,
        .points = (GPoint[]){ { frame.size.w - 3 - 23, 5 },
                              { frame.size.w - 3, 5 },
                              { frame.size.w - 3, frame.size.h - 3 },
                              { frame.size.w - 3 - 23, frame.size.h - 3 } }
    };
    GPath *gpath = gpath_create(&border);
    gpath_draw_outline(ctx, gpath);
    gpath_destroy(gpath);
    graphics_draw_line(ctx, (GPoint){ frame.size.w - 1, 7}, (GPoint){ frame.size.w - 1, 11 });
    graphics_draw_line(ctx, (GPoint){ frame.size.w - 2, 7}, (GPoint){ frame.size.w - 2, 11 });
    // Battery icon level
    BatteryChargeState state = battery_state_service_peek();
    int chargePercent = state.charge_percent / 10;
    GPathInfo level = {
        .num_points = 4,
        .points = (GPoint[]){ { border.points[0].x + 1, border.points[0].y + 2 },
                              { border.points[0].x + 1 + 2 * chargePercent + 1, border.points[1].y + 2 },
                              { border.points[0].x + 1 + 2 * chargePercent + 1, border.points[2].y - 2 },
                              { border.points[3].x + 1, border.points[3].y - 2 } }
    };
    gpath = gpath_create(&level);
    gpath_draw_filled(ctx, gpath);
    gpath_destroy(gpath);
}

//------------------------------------------------------------------------
//
// App sync handling
//

enum {
    APPSYNC_SCREEN_SAVER = 0x01,
    APPSYNC_UPTIME,
    APPSYNC_BATTERY_PERCENT,
    APPSYNC_BATTERY_ICON,
};

AppSync sync;
uint8_t syncBuffer[64];

void syncTupleDidFail(DictionaryResult dictResult, AppMessageResult msgResult, void *context)
{
    DLOG("App Message Sync Error: %d", msgResult);
}

void syncTupleDidChange(const uint32_t key, const Tuple *new_tuple, const Tuple *old_tuple, void *context)
{
    switch (key) {
        case APPSYNC_SCREEN_SAVER: {
            useScreenSaver = (strcmp(new_tuple->value->cstring, "on") == 0);
            status_t rc = persist_write_string(APPSYNC_SCREEN_SAVER, new_tuple->value->cstring);
            DLOG("Screen saver = %s, %d", new_tuple->value->cstring, (int)rc);
            break;
        }

        case APPSYNC_UPTIME: {
            showUptime = new_tuple->value->uint8;
            status_t rc = persist_write_bool(APPSYNC_UPTIME, (bool)new_tuple->value->uint8);
            BatteryChargeState state = battery_state_service_peek();
            batteryStateHandler(state);
            DLOG("Uptime = %d, %d", new_tuple->value->uint8, (int)rc);
            break;
        }

        case APPSYNC_BATTERY_PERCENT: {
            showBatteryPercent = new_tuple->value->uint8;
            status_t rc = persist_write_bool(APPSYNC_BATTERY_PERCENT, (bool)new_tuple->value->uint8);
            BatteryChargeState state = battery_state_service_peek();
            batteryStateHandler(state);
            DLOG("Battery percent = %d, %d", new_tuple->value->uint8, (int)rc);
            break;
        }

        case APPSYNC_BATTERY_ICON: {
            showBatteryIcon = new_tuple->value->uint8;
            status_t rc = persist_write_bool(APPSYNC_BATTERY_ICON, (bool)new_tuple->value->uint8);
            layer_mark_dirty(batteryIcon);
            DLOG("Battery icon = %d, %d", new_tuple->value->uint8, (int)rc);
            break;
        }
    }
}

//------------------------------------------------------------------------
//
// Time handling
//

void handle_init(void)
{
    GFont date_font = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_CHECKBOOK_24));
    GFont time_font = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_CHECKBOOK_TIME_42));

    window = window_create();
    Layer *rootLayer = window_get_root_layer(window);
    screenSaverInit();

    window_set_background_color(window, GColorBlack);

    // Initialize status bar
    statusLabel = text_layer_create(statusBarFrame);
    text_layer_set_text_color(statusLabel, GColorWhite);
    text_layer_set_background_color(statusLabel, GColorClear);
    text_layer_set_font(statusLabel, fonts_get_system_font(FONT_KEY_GOTHIC_14));
    text_layer_set_text_alignment(statusLabel, GTextAlignmentLeft);
    layer_add_child(rootLayer, text_layer_get_layer(statusLabel));
    GRect batteryLabelFrame = statusBarFrame;
    batteryLabelFrame.size.w -= 30;
    batteryLabel = text_layer_create(batteryLabelFrame);
    text_layer_set_text_color(batteryLabel, GColorWhite);
    text_layer_set_background_color(batteryLabel, GColorClear);
    text_layer_set_font(batteryLabel, fonts_get_system_font(FONT_KEY_GOTHIC_14));
    text_layer_set_text_alignment(batteryLabel, GTextAlignmentRight);
    layer_add_child(rootLayer, text_layer_get_layer(batteryLabel));
    batteryIcon = layer_create(statusBarFrame);
    layer_set_update_proc(batteryIcon, batteryIconUpdate);
    layer_add_child(rootLayer, batteryIcon);

    // Initialize weekday label
    weekdayLabel = text_layer_create(GRect(0, screenFrame.size.h / 8 + statusBarFrame.size.h / 2, screenFrame.size.w, screenFrame.size.h / 4));
    text_layer_set_text_color(weekdayLabel, GColorWhite);
    text_layer_set_background_color(weekdayLabel, GColorClear);
    text_layer_set_font(weekdayLabel, date_font);
    text_layer_set_text_alignment(weekdayLabel, GTextAlignmentCenter);
    layer_add_child(rootLayer, text_layer_get_layer(weekdayLabel));

    // Initialize date label
    dateLabel = text_layer_create(GRect(0, screenFrame.size.h * 6 / 8 + statusBarFrame.size.h / 2, screenFrame.size.w, screenFrame.size.h / 4));
    text_layer_set_text_color(dateLabel, GColorWhite);
    text_layer_set_background_color(dateLabel, GColorClear);
    text_layer_set_font(dateLabel, date_font);
    text_layer_set_text_alignment(dateLabel, GTextAlignmentCenter);
    layer_add_child(rootLayer, text_layer_get_layer(dateLabel));

    // Initialize time label
    timeLabel = text_layer_create(GRect(0, screenFrame.size.h * 3 / 8 + statusBarFrame.size.h / 2, screenFrame.size.w, screenFrame.size.h / 2));
    text_layer_set_text_color(timeLabel, GColorWhite);
    text_layer_set_background_color(timeLabel, GColorClear);
    text_layer_set_font(timeLabel, time_font);
    text_layer_set_text_alignment(timeLabel, GTextAlignmentCenter);
    layer_add_child(rootLayer, text_layer_get_layer(timeLabel));

    // Configure button click handlers; only works in apps
    //window_set_click_config_provider(&window, (ClickConfigProvider)clickConfiguration);

    // Read watch state from persistent storage
    if (persist_exists(PERSIST_TIMESTAMP)) {
        timestamp = persist_read_int(PERSIST_TIMESTAMP);
        if (timestamp == 0) {
            time(&timestamp);
        }
        //snprintf(uptime, sizeof(uptime), "%d", (int)timestamp);
        //text_layer_set_text(statusLabel, uptime);
    }
    else {
        // Use the current time
        time(&timestamp);
        persist_write_int(PERSIST_TIMESTAMP, timestamp);
        vibes_long_pulse();
    }

    window_stack_push(window, true);
    tick_timer_service_subscribe(MINUTE_UNIT, handle_tick_event);
    accel_tap_service_subscribe(accelTapHandler);

    // Initialize the status bar with the current battery state
    BatteryChargeState state = battery_state_service_peek();
    batteryStateHandler(state);
    battery_state_service_subscribe(batteryStateHandler);

    // Initialize configuration options
    if (persist_exists(APPSYNC_SCREEN_SAVER)) {
        char buffer[4];
        persist_read_string(APPSYNC_SCREEN_SAVER, buffer, sizeof(buffer));
        useScreenSaver = (strcmp(buffer, "on") == 0);
        showUptime = persist_read_bool(APPSYNC_UPTIME);
        showBatteryPercent = persist_read_bool(APPSYNC_BATTERY_PERCENT);
        showBatteryIcon = persist_read_bool(APPSYNC_BATTERY_ICON);
        DLOG("Saved configuration: %d %d %d %d", useScreenSaver, showUptime, showBatteryPercent, showBatteryIcon);
    }
    else {
        DLOG("No saved configuration saved in persistent storage %s", "");
    }
    app_message_open(64, 64);
    Tuplet initialConfiguration[] = {
        TupletCString(APPSYNC_SCREEN_SAVER, (useScreenSaver ? "on" : "off")),
        TupletInteger(APPSYNC_UPTIME, (uint8_t)showUptime),
        TupletInteger(APPSYNC_BATTERY_PERCENT, (uint8_t)showBatteryPercent),
        TupletInteger(APPSYNC_BATTERY_ICON, (uint8_t)showBatteryIcon),
    };
    app_sync_init(&sync, syncBuffer, sizeof(syncBuffer), initialConfiguration, ARRAY_LENGTH(initialConfiguration),
                  syncTupleDidChange, syncTupleDidFail, NULL);
}

void handle_deinit(void)
{
    tick_timer_service_unsubscribe();
    accel_tap_service_unsubscribe();
    battery_state_service_unsubscribe();

    window_destroy(window);
    text_layer_destroy(statusLabel);
    text_layer_destroy(batteryLabel);
    text_layer_destroy(weekdayLabel);
    text_layer_destroy(dateLabel);
    text_layer_destroy(timeLabel);
    screenSaverDeinit();
}

void handle_tick_event(struct tm *time, TimeUnits unitsChanged)
{
    //presentWindow(screenSaver);
    //return;
#ifdef DEBUG
    // Toggle screen saver mode every third minute
    if (unitsChanged & MINUTE_UNIT) {
        if ((time->tm_min % 3) == 0) {
#else
    // If the hour has changed
    if (unitsChanged & HOUR_UNIT) {
        // and it's between 1:00am-7:00am; don't use 12:00am because we check date changes below
        if (time->tm_hour > 0 && time->tm_hour < 7) {
#endif
            // and the screen saver is not showing
            if (useScreenSaver && window_stack_get_top_window() != screenSaver) {
                // present it and return immediately
                presentWindow(screenSaver);
                return;
            }
        }
        else {
            // If it's after 7:00am and the screen saver is showing, dismiss it
            if (window_stack_get_top_window() == screenSaver) {
                dismissWindow();
            }
        }
    }

    // If the screen saver is showing, abort the tick handler
    if (window_stack_get_top_window() == screenSaver) {
        return;
    }

    // Need to be static because they're used by the system later.
    static char time_text[] = "00:00a";  // 12:34a
    static char weekday_text[] = "Wednesday"; // Longest weekday string
    static char date_text[] = "September 30"; // Longest date string

    static bool date_initialized = false;

    // Check if the date label needs to be updated
    if (!date_initialized || (unitsChanged & DAY_UNIT)) {
        date_initialized = true;
        strftime(weekday_text, sizeof(weekday_text), "%A", time);
        //strcpy(weekday_text, "WEDNESDAY");  // Widest characters for weekday
        // Convert to all uppercase characters for a more readable display
        for (char *s = &weekday_text[0]; *s != '\0'; ++s) {
            *s = toupper((int)*s);
        }
        text_layer_set_text(weekdayLabel, weekday_text);

        strftime(date_text, sizeof(date_text), "%B %e", time);
        //strcpy(date_text, "JANUARY 8"); // Widest nine characters for date
        // Since uppercase characters are wider, convert only if the date fits
        if (strlen(date_text) < 10) {
            for (char *s = &date_text[0]; *s != '\0'; ++s) {
                *s = toupper((int)*s);
            }
        }
        text_layer_set_text(dateLabel, date_text);
    }

    if (clock_is_24h_style()) {
        strftime(time_text, sizeof(time_text), "%R", time);
    }
    else {
        // Append a 'a' or 'p', because %P is not supported
        if (time->tm_hour < 12) {
            strftime(time_text, sizeof(time_text), "%I:%Ma", time);
        }
        else {
            strftime(time_text, sizeof(time_text), "%I:%Mp", time);
        }
        // Kludge to handle lack of non-padded hour format string
        // for twelve hour clock.
        if (time_text[0] == '0') {
            memmove(time_text, &time_text[1], sizeof(time_text) - 1);
        }
    }

    text_layer_set_text(timeLabel, time_text);
    //text_layer_set_text(timeLabel, "4:24p");
}

//------------------------------------------------------------------------

int main(void)
{
    handle_init();
    app_event_loop();
    handle_deinit();
}
